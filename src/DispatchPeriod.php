<?php declare(strict_types=1);

namespace App;

use App\Consignment\Consignment;

/**
 * Class DispatchPeriod
 * @package App
 */
class DispatchPeriod implements DispatchPeriodInterface
{
    private string $startTime;

    private string $endTime;

    private $consignments;

    public function __construct()
    {
        $this->consignments = [];
    }

    /**
     * Do some actions on batch start
     */
    public function startBatch(): void
    {
        $this->startTime = 'some-date-time';
    }

    /**
     * Do some actions on batch end
     */
    public function endBatch(): void
    {
        $this->endTime = 'some-date-time';
    }

    /**
     * @param Consignment $consignment
     */
    public function addConsignment(Consignment $consignment): void
    {
        // @todo: add checks if it's not closed
        $this->consignments[] = $consignment;
    }

    /**
     * @return array
     */
    public function getConsignments(): array
    {
        return $this->consignments;
    }
}