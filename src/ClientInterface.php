<?php declare(strict_types=1);

namespace App;

use App\Consignment\Consignment;

/**
 * Interface ClientInterface
 * @package App
 */
interface  ClientInterface
{
    public function startNewBatch(): void;

    public function endCurrentBatch(): void;

    public function addConsignment(Consignment $consignment): void;
}