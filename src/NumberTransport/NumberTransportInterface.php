<?php declare(strict_types=1);

namespace App\NumberTransport;

/**
 *
 * Interface NumberTransportInterface
 * @package App\Consignment
 */
interface NumberTransportInterface
{
    public function transport(array $numbers): void;
}