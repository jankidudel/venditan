<?php declare(strict_types=1);

namespace App\NumberTransport;

/**
 * To send numbers via anonymous FTP
 * Class NumberTransportAnonymousFtp
 * @package App
 */
class NumberTransportAnonymousFtp implements NumberTransportInterface
{
    public function transport(array $numbers): void
    {
        // TODO: Implement transport() method.
    }
}