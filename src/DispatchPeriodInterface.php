<?php declare(strict_types=1);

namespace App;

use App\Consignment\Consignment;

/**
 * Interface for the period's actions
 *
 * Interface DispatchPeriodInterface
 * @package App
 */
interface DispatchPeriodInterface
{
    public function startBatch(): void;

    public function endBatch(): void;

    public function addConsignment(Consignment $consignment): void;

    public function getConsignments(): array;

}