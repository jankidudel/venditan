<?php declare(strict_types=1);

namespace App;

use App\Consignment\Consignment;

/**
 * Class Client
 * Used for client interface, should probably be something like facade pattern
 * @package App
 */
class Client implements ClientInterface
{
    /** @var DispatchPeriodInterface $dispatchPeriod */
    private DispatchPeriodInterface $dispatchPeriod;

    public function __construct(DispatchPeriodInterface $dispatchPeriod)
    {
        $this->dispatchPeriod = $dispatchPeriod;
    }

    public function startNewBatch(): void
    {
        $this->dispatchPeriod->startBatch();
    }

    public function endCurrentBatch(): void
    {
        $this->dispatchPeriod->endBatch();

        // TODO: develop a good way to keep all & retrieve consignments with every courier
        foreach ($this->dispatchPeriod->getConsignments() as $consignment) {
            $consignment->getCourier()->transport();
        }
    }

    public function addConsignment(Consignment $consignment): void
    {
        $this->dispatchPeriod->addConsignment($consignment);
    }
}