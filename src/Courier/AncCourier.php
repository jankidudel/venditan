<?php declare(strict_types=1);

namespace App\Courier;

/**
 * Class AncCourier
 * @package App\Courier
 */
class AncCourier extends AbstractCourier
{
    public function generateNumber(): string
    {
        // TODO: will generate unique number for ANC
    }
}