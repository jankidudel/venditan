<?php declare(strict_types=1);

namespace App\Courier;

/**
 * Interface ConsignmentNumberGeneratorInterface
 * @package App\Consignment
 */
interface ConsignmentNumberGeneratorInterface
{
    public function generateNumber(): string;
}