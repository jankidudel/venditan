<?php declare(strict_types=1);

namespace App\Courier;

use App\NumberTransport\NumberTransportInterface;

/**
 * Base class for various couriers, using numberTransport provided by specific courier
 * Using kind-of strategy pattern
 * Class AbstractCourier
 * @package App\Courier
 */
abstract class AbstractCourier implements ConsignmentNumberGeneratorInterface
{
    /**
     * @var NumberTransportInterface
     */
    protected NumberTransportInterface $numberTransport;

    /**
     * AbstractCourier constructor.
     * @param NumberTransportInterface $numberTransport
     */
    public function __construct(NumberTransportInterface $numberTransport)
    {
        $this->numberTransport = $numberTransport;
    }

    /**
     * @param array $numbers
     */
    public function transport(array $numbers): void
    {
        $this->numberTransport->transport($numbers);
    }
}