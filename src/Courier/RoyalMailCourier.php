<?php declare(strict_types=1);

namespace App\Courier;

/**
 * Class RoyalMailCourier
 * @package App\Courier
 */
class RoyalMailCourier extends AbstractCourier
{
    public function generateNumber(): string
    {
        // TODO: will generate unique number for Royal Mail
    }
}