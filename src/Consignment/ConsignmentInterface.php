<?php declare(strict_types=1);

namespace App\Consignment;

use App\Courier\AbstractCourier;
use App\Parcel;

/**
 * Interface ConsignmentInterface
 * @package App\Consignment
 */
interface ConsignmentInterface
{
    public function getCourier(): AbstractCourier;

    public function setParcel(Parcel $parcel): void;
}