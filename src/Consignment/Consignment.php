<?php declare(strict_types=1);

namespace App\Consignment;

use App\Courier\AbstractCourier;
use App\Parcel;

/**
 * Every consignment has a unique number, courier and parcel to send
 * Class Consignment
 * @package App\Consignment
 */
class Consignment implements ConsignmentInterface
{
    private string $uniqueNumber;
    private ?Parcel $parcel;
    private ?AbstractCourier $courier;

    /**
     * Consignment constructor.
     * @param AbstractCourier $courier
     */
    public function __construct(AbstractCourier $courier)
    {
        $this->courier = $courier;
        $this->uniqueNumber = $this->courier->generateNumber();
    }

    /**
     * @return AbstractCourier
     */
    public function getCourier(): AbstractCourier
    {
        return $this->courier;
    }

    /**
     * @param Parcel $parcel
     */
    public function setParcel(Parcel $parcel): void
    {
        // TODO: Implement setParcel() method.
    }
}